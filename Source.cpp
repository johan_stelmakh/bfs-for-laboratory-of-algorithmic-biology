#include <iostream>
#include <queue> // queue
using namespace std;

int main()
{
	queue<int> Queue;
	int mas[7][7] = { { 0, 1, 1, 0, 0, 0, 1 }, // adjacency matrix
	{ 1, 0, 1, 1, 0, 0, 0 },
	{ 1, 1, 0, 0, 0, 0, 0 },
	{ 0, 1, 0, 0, 1, 0, 0 },
	{ 0, 0, 0, 1, 0, 1, 0 },
	{ 0, 0, 0, 0, 1, 0, 1 },
	{ 1, 0, 0, 0, 0, 1, 0 } };
	int nodes[7]; // graph nodes
	for (int i = 0; i < 7; i++)
		nodes[i] = 0; // initially all vertices are 0
	int target_node = 7; // set the target-node
	Queue.push(0); // put the first node into the queue
	while (!Queue.empty())
	{ // while the queue is not empty
		int node = Queue.front(); // extract the node
		Queue.pop();
		nodes[node] = 2; // point it as visited one
		if (target_node == node + 1)
		{
			break; // if this one was the target one, end the cycle immediately
		}
		for (int j = 0; j < 7; j++)
		{ // check all adjacent vertices for our node
			if (mas[node][j] == 1 && nodes[j] == 0)
			{ // if the node is adjacent and not found
				Queue.push(j); // add it to the queue
				nodes[j] = 1; // and mark it as discovered
			}
		}
		cout << node + 1 << endl; // output the node number
	}
	cin.get(); // just a way to hold the console
	return 0;
}